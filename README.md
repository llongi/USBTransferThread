USBTransferThread
=================

High performance data exchange via USB for Java, using a thread and the libusb4java library.
