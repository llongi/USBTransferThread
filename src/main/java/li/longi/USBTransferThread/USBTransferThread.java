package li.longi.USBTransferThread;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.usb4java.BufferUtils;
import org.usb4java.Context;
import org.usb4java.DeviceHandle;
import org.usb4java.LibUsb;
import org.usb4java.Transfer;
import org.usb4java.TransferCallback;

public final class USBTransferThread extends Thread {
	private final class ResubmitTransfer implements TransferCallback {
		@Override
		public void processTransfer(final Transfer transfer) {
			// Update buffer limit to actual data read, if successfully transferred.
			if ((transfer.status() == LibUsb.TRANSFER_COMPLETED) || (transfer.status() == LibUsb.TRANSFER_CANCELLED)) {
				transfer.buffer().limit(transfer.actualLength());
			}

			callback.processTransfer(new RestrictedTransfer(transfer));

			// Directly resubmit Transfers once processed by user.
			if ((transfer.status() == LibUsb.TRANSFER_COMPLETED) && !Thread.currentThread().isInterrupted()) {
				// Reset buffer to initial state.
				// usb4java never touches the buffer internals, neither do we, but the user callback might!
				transfer.buffer().clear();

				// Execute any further preparation steps (like loading the buffer for OUT transfers).
				callback.prepareTransfer(new RestrictedTransfer(transfer));

				// Submit transfer again.
				if (LibUsb.submitTransfer(transfer) == LibUsb.SUCCESS) {
					return;
				}
			}

			// Cannot recover (cancelled, no device, or stalled).
			// Signal this by adjusting the counter, free and exit.
			transfers.remove(transfer);
			LibUsb.freeTransfer(transfer);
		}
	}

	public static final int DEFAULT_BUFFER_NUMBER = 16;
	public static final int DEFAULT_BUFFER_SIZE = 8192;

	private int bufferNumber = USBTransferThread.DEFAULT_BUFFER_NUMBER;
	private int bufferSize = USBTransferThread.DEFAULT_BUFFER_SIZE;

	private final List<Transfer> transfers = Collections.synchronizedList(new LinkedList<Transfer>());

	private final AtomicBoolean updateConfiguration = new AtomicBoolean();

	private final DeviceHandle device;
	private final Context context;
	private final byte endpoint;
	private final byte type;
	private final RestrictedTransferCallback callback;
	private final Runnable setupCallback;
	private final Runnable shutdownCallback;
	private final Runnable exceptionalShutdownCallback;

	public USBTransferThread(final DeviceHandle device, final byte endpoint, final byte type,
		final RestrictedTransferCallback callback) {
		this(device, endpoint, type, callback, 0, 0); // Use default buffer number and size.
	}

	public USBTransferThread(final DeviceHandle device, final byte endpoint, final byte type,
		final RestrictedTransferCallback callback, final int bufferNumber, final int bufferSize) {
		this(device, endpoint, type, callback, bufferNumber, bufferSize, null, null, null);
	}

	public USBTransferThread(final DeviceHandle device, final byte endpoint, final byte type,
		final RestrictedTransferCallback callback, final int bufferNumber, final int bufferSize,
		final Runnable setupCallback, final Runnable shutdownCallback, final Runnable exceptionalShutdownCallback) {
		this(device, endpoint, type, callback, bufferNumber, bufferSize, setupCallback, shutdownCallback,
			exceptionalShutdownCallback, null);
	}

	public USBTransferThread(final DeviceHandle device, final byte endpoint, final byte type,
		final RestrictedTransferCallback callback, final int bufferNumber, final int bufferSize,
		final Runnable setupCallback, final Runnable shutdownCallback, final Runnable exceptionalShutdownCallback,
		final Context context) {
		// Validate input.
		if (device == null) {
			throw new IllegalArgumentException("device cannot be null");
		}
		if ((type != LibUsb.TRANSFER_TYPE_BULK) && (type != LibUsb.TRANSFER_TYPE_INTERRUPT)) {
			throw new IllegalArgumentException("only BULK and INTERRUPT transfers are supported");
		}
		if (callback == null) {
			throw new IllegalArgumentException("callback cannot be null");
		}

		this.device = device;
		this.context = context;
		this.endpoint = endpoint;
		this.type = type;
		this.callback = callback;

		this.setupCallback = setupCallback;
		this.shutdownCallback = shutdownCallback;
		this.exceptionalShutdownCallback = exceptionalShutdownCallback;

		setBufferNumber(bufferNumber);
		setBufferSize(bufferSize);

		// Set name to something known and useful, and ensure the USB buffer handling thread has maximum priority as to
		// avoid lag situations (especially on Windows).
		setName("USBTransferThread");
		setPriority(Thread.MAX_PRIORITY);
	}

	public synchronized int getBufferNumber() {
		return bufferNumber;
	}

	public synchronized void setBufferNumber(final int bufNumber) {
		// No negative values.
		if (bufNumber < 0) {
			throw new IllegalArgumentException("bufNumber can't be smaller than 0");
		}

		// Keep old buffer number around for later.
		final int oldBufferNumber = bufferNumber;

		// Update buffer number. 0 means to use default number.
		if (bufNumber == 0) {
			bufferNumber = USBTransferThread.DEFAULT_BUFFER_NUMBER;
		}
		else {
			bufferNumber = bufNumber;
		}

		// If the number didn't change, don't do anything.
		// Also, if there are no transfers at all or the thread is interrupted, don't do anything.
		// This happens only during shutdown, or before startup (no transfers yet)!
		if ((oldBufferNumber != bufferNumber) && !Thread.currentThread().isInterrupted()) {
			updateConfiguration.set(true);
		}
	}

	public synchronized int getBufferSize() {
		return bufferSize;
	}

	public synchronized void setBufferSize(final int bufSize) {
		// No negative values.
		if (bufSize < 0) {
			throw new IllegalArgumentException("bufSize can't be smaller than 0");
		}

		// Keep old buffer size around for later.
		final int oldBufferSize = bufferSize;

		// Update buffer size. 0 means to use default size.
		if (bufSize == 0) {
			bufferSize = USBTransferThread.DEFAULT_BUFFER_SIZE;
		}
		else {
			bufferSize = bufSize;
		}

		// If the size didn't change, don't do anything.
		// Also, if there are no transfers at all or the thread is interrupted, don't do anything.
		// This happens only during shutdown, or before startup (no transfers yet)!
		if ((oldBufferSize != bufferSize) && !Thread.currentThread().isInterrupted()) {
			updateConfiguration.set(true);
		}
	}

	@Override
	public synchronized void interrupt() {
		// Synchronize interrupt() with the above buffer related calls, so that we never get interrupted in the middle
		// of canceling and re-creating all the Transfers and their buffers.
		super.interrupt();
	}

	private void allocateTransfers(final int bufNum, final int bufSize) {
		for (int i = 0; i < bufNum; i++) {
			final Transfer transfer = LibUsb.allocTransfer();
			if (transfer == null) {
				throw new IllegalStateException(String.format("could not allocate new transfer %d", i));
			}

			final ByteBuffer buffer = BufferUtils.allocateByteBuffer(bufSize);
			if (buffer == null) {
				LibUsb.freeTransfer(transfer);
				throw new IllegalStateException(
					String.format("could not allocate %d bytes buffer for new transfer %d", bufSize, i));
			}

			// Initialize Transfer.
			transfer.setDevHandle(device);
			transfer.setEndpoint(endpoint);
			transfer.setType(type);
			transfer.setBuffer(buffer);
			transfer.setCallback(new ResubmitTransfer());
			transfer.setUserData(null);
			transfer.setTimeout(0);

			// Execute any further preparation steps (like loading the buffer for OUT transfers).
			callback.prepareTransfer(new RestrictedTransfer(transfer));

			// Submit Transfer.
			final int status = LibUsb.submitTransfer(transfer);
			if (status == LibUsb.SUCCESS) {
				// Add initialized Transfer to end of the global list.
				transfers.add(transfer);
			}
			else {
				LibUsb.freeTransfer(transfer);
				throw new IllegalStateException(String.format("could not submit transfer %s, error: %d - %s", transfer,
					status, LibUsb.errorName(status)));
			}
		}

		if (transfers.isEmpty()) {
			// Failed to allocate USB transfers, signal this.
			throw new IllegalStateException("could not allocate any transfers");
		}
	}

	private void deallocateTransfers() {
		// Cancel all transfers on the global list, that have not been cancelled yet.
		synchronized (transfers) {
			for (final Transfer t : transfers) {
				// Cancel all transfers, don't care about errors.
				LibUsb.cancelTransfer(t);
			}
		}

		// Wait for all transfers to go away (0.1 seconds timeout).
		while (!transfers.isEmpty()) {
			LibUsb.handleEventsTimeout(context, 100000);
		}
	}

	@Override
	public void run() {
		if (setupCallback != null) {
			setupCallback.run();
		}

		updateConfiguration.set(false);
		allocateTransfers(getBufferNumber(), getBufferSize());

		// Handle events in a loop until interrupted, or if no more transfers alive: device is gone.
		while (!Thread.currentThread().isInterrupted() && !transfers.isEmpty()) {
			if (updateConfiguration.getAndSet(false)) {
				// Configuration (buffer number, size) changed, reallocate them.
				deallocateTransfers();
				allocateTransfers(getBufferNumber(), getBufferSize());
			}

			// Handle events with a 1.0 second timeout (in microseconds).
			LibUsb.handleEventsTimeout(context, 1000000);
		}

		deallocateTransfers();

		// Try to distinguish if we terminated normally in response to an interrupt request,
		// or if we exited abnormally by loosing all our active transfers.
		if (Thread.currentThread().isInterrupted()) {
			if (shutdownCallback != null) {
				shutdownCallback.run();
			}
		}
		else {
			if (exceptionalShutdownCallback != null) {
				exceptionalShutdownCallback.run();
			}
		}
	}
}
