package li.longi.USBTransferThread;

public interface RestrictedTransferCallback {
	void prepareTransfer(RestrictedTransfer transfer);

	void processTransfer(RestrictedTransfer transfer);
}
